#!/bin/bash
################################################################
##
##   Ustawienia
##
################################################################
PROJECT_NAME="project"
PROJECT_PATH="/root/repos/project"
DATE_TIME=`date +"%Y-%m-%d-%H-%M-%S"`
DB_BACKUP_PATH_TEMP="/opt/backup-${PROJECT_NAME}"
################################################################
##
##   Tworzenie folderu tymczasowego
##
################################################################
mkdir -p ${DB_BACKUP_PATH_TEMP}
################################################################
##
##   Tworzenie kopii bazy danych
##
################################################################
cd ${PROJECT_PATH}
make db-dump
################################################################
##
##   Kompresja projektu
##
################################################################
tar -zcf ${DB_BACKUP_PATH_TEMP}/${PROJECT_NAME}-${DATE_TIME}.tar.gz ${PROJECT_PATH}
################################################################
##
##   Upload na ftp
##   curl https://rclone.org/install.sh | bash
##
################################################################
rclone copy ${DB_BACKUP_PATH_TEMP}/${PROJECT_NAME}-${DATE_TIME}.tar.gz FTP:/${PROJECT_NAME}/
################################################################
##
##   Usuwanie pliku tymczasowego
##
################################################################
rm -f ${DB_BACKUP_PATH_TEMP}/${PROJECT_NAME}-${DATE_TIME}.tar.gz
