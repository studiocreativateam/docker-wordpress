### Usage

1. Copy env file: `cp .env.example .env`
2. Copy parameters file: `cp ./wordpress/.htaccess.example ./wordpress/.htaccess`
3. Copy defines file: `cp ./wordpress/wp-config.example.php ./wordpress/wp-config.php`
4. Copy parameters file: `cp ./docker/php.ini.example ./docker/php.ini`
5. Run command `make up` for linux/mac-os or `make up-win` for windows and wait up to 2 mins after first run.
6. Find `themes/main-theme` throughout your project and replace it with your theme name
7. Install npm `make bash` => `cd wp-content/themes/main-theme` => `npm install`
8. Compile npm `make bash` => `cd wp-content/themes/main-theme` => `npm run build`
9. Go to website http://localhost <- port from .env file.
10. Go to phpmyadmin http://localhost:8080 <- port from .env file.

#### Usefully commands
- Docker start: `make up` or `make up-win`
- Docker stop: `make down` or `make down-win`
- Go do bash: `make bash` or `make bash-win`
- Make database backup: `make db-dump`

#### Import DB
docker exec -i wp_project_mysql mysql -uwordpress -psecret wordpress < dumps/dump.sql
